package net.maiatoday.lunosample

import io.github.aakira.napier.Napier
import io.ktor.client.call.body
import io.ktor.client.plugins.contentnegotiation.ContentNegotiation
import io.ktor.client.plugins.logging.*
import io.ktor.client.request.get
import kotlinx.serialization.Serializable
import io.ktor.serialization.kotlinx.json.json
import kotlinx.serialization.json.Json
import net.maiatoday.lunosample.repository.*

@Serializable
data class Hello(
    val string: String,
    val lang: String
)

object Greeting {
    private val queries = createDatabase().lunosampleQueries
    private val httpClient = httpClient() {
        install(
            Logging
        ) {
            level = LogLevel.HEADERS
            logger = object : Logger {
                override fun log(message: String) {
                    Napier.v(tag = "HTTP Client", message = message)
                }
            }
        }
        install(ContentNegotiation) {
            json(
                Json {
                    ignoreUnknownKeys = true
                }
            )
        }
    }.also { initLogger() }

    @Throws(Throwable::class)
    suspend fun greeting(): String {
        // mashed up example hooking up piece with no architecture yet...
        // add value to db
        val fetchedList = getHello()
        fetchedList.forEach {
            queries.insertItem(it.string, it.lang)
        }

        // get values from db
        val allHello = queries.selectAll().executeAsList()
        return "Hello/${allHello.random().string}, ${Platform().platform}!"
    }

    private suspend fun getHello(): List<Hello> {
        return httpClient.get("https://gitcdn.link/cdn/KaterinaPetrova/greeting/7d47a42fc8d28820387ac7f4aaf36d69e434adc1/greetings.json")
            .body()
    }

}