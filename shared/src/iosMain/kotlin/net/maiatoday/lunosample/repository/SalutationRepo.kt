package net.maiatoday.lunosample.repository

import com.squareup.sqldelight.drivers.native.NativeSqliteDriver
import net.maiatoday.lunosample.LunoSampleDatabase

actual fun createDatabase(): LunoSampleDatabase {
    val driver = NativeSqliteDriver(LunoSampleDatabase.Schema, "salutation.db")
    return LunoSampleDatabase(driver)
}