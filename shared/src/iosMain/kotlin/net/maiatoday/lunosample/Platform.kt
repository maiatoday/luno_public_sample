package net.maiatoday.lunosample

import platform.UIKit.UIDevice
import io.github.aakira.napier.DebugAntilog
import io.github.aakira.napier.Napier
import io.ktor.client.*

actual class Platform actual constructor() {
    actual val platform: String =
        UIDevice.currentDevice.systemName() + " " + UIDevice.currentDevice.systemVersion
}

actual fun httpClient(config: HttpClientConfig<*>.() -> Unit) = HttpClient() {
    config(this)

//    engine {
//        configureRequest {
//            setAllowsCellularAccess(true)
//        }
//    }
}

actual fun initLogger() {
    Napier.base(DebugAntilog())
}
