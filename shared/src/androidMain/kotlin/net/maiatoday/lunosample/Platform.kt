package net.maiatoday.lunosample

import android.content.Context
import com.squareup.sqldelight.android.AndroidSqliteDriver
import io.github.aakira.napier.DebugAntilog
import io.github.aakira.napier.Napier
import io.ktor.client.*

actual class Platform actual constructor() {

    actual val platform: String = "Android ${android.os.Build.VERSION.SDK_INT}"
}

actual fun httpClient(config: HttpClientConfig<*>.() -> Unit) = HttpClient() {
    config(this)

//    engine {
//        config {
//            retryOnConnectionFailure(true)
//            connectTimeout(5, TimeUnit.SECONDS)
//        }
//    }
}

actual fun initLogger() {
    Napier.base(DebugAntilog())
}