package net.maiatoday.lunosample.repository

import android.content.Context
import com.squareup.sqldelight.android.AndroidSqliteDriver
import net.maiatoday.lunosample.LunoSampleDatabase

lateinit var appContext: Context

actual fun createDatabase(): LunoSampleDatabase {
    val driver = AndroidSqliteDriver(LunoSampleDatabase.Schema, appContext, "salutation.db")
    return LunoSampleDatabase(driver)
}