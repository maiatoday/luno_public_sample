package net.maiatoday.lunosample.android

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import net.maiatoday.lunosample.Greeting
import android.widget.TextView
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch

suspend fun greet(): String {
    return Greeting.greeting()
}

class MainActivity : AppCompatActivity() {

    private val mainScope = MainScope()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val tv: TextView = findViewById(R.id.text_view)
        mainScope.launch {
            kotlin.runCatching {
                greet()
            }.onSuccess {
                tv.text = it
            }.onFailure {
                tv.text = it.localizedMessage
            }
        }
    }
}
