package net.maiatoday.lunosample.android
import android.app.Application
import net.maiatoday.lunosample.repository.appContext

class LunoSampleApp : Application() {

    override fun onCreate() {
        super.onCreate()
        appContext = this
    }
}